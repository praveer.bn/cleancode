package com.CleanCoding.Clean.Coding.service.impl;

import com.CleanCoding.Clean.Coding.entity.Product;
import com.CleanCoding.Clean.Coding.exceptions.ResourceNotFoundException;
import com.CleanCoding.Clean.Coding.repository.ProductRepository;
import com.CleanCoding.Clean.Coding.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Override
    public Product addProduct(Product product) {
        Product savedProduct = productRepository.save(product);
        return savedProduct;
    }

    @Override
    public List<Product> getAllProducts() {
        List<Product> productList = productRepository.findAll();
        return productList;
    }

    @Override
    public Product getProductById(Long productId) {
        Product product = productRepository.findById(productId).orElseThrow(() -> new ResourceNotFoundException("Product not found By This Id: " + productId));
        return product;
    }

    @Override
    public Product updateProduct(Product newProduct) {
        Product product = productRepository.findById(newProduct.getProductId()).orElseThrow(() -> new ResourceNotFoundException("Product not found By This Id: " + newProduct.getProductId()));
        if (newProduct.getProductName().isEmpty()) {
            product.setProductName(newProduct.getProductName());
        }
        if (newProduct.getProductDescription().isEmpty()) {
            product.setProductDescription(newProduct.getProductDescription());
        }
        if (newProduct.getProductPrice()!=null) {
            product.setProductPrice(newProduct.getProductPrice());
        }
        if (newProduct.getQuantity()!= null) {
            product.setQuantity(newProduct.getQuantity());
        }
        return productRepository.save(product);
    }
}

