package com.CleanCoding.Clean.Coding.service;

import com.CleanCoding.Clean.Coding.entity.Product;

import java.util.List;

public interface ProductService {
    Product addProduct(Product product);

    List<Product> getAllProducts();

    Product getProductById(Long productId);

    Product updateProduct(Product newProduct);
}
